import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { GlobalStyles } from "./GlobalStyles";
import LandingPage from "./pages/LandingPage/LandingPage";

const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<LandingPage />} />
			</Routes>
			<GlobalStyles/>
		</BrowserRouter>
	);
}

export default App;
