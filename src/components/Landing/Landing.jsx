import React from 'react'
import logo from "../../assets/images/logo.svg"
import { Wrapper } from './Landing.styles';

const Landing = () => {
    return (
        <Wrapper>
            <img src={logo} alt="logo" />
            <p>
                Edit <code>components/Landing/Landing.jsx</code> and save to reload.
            </p>
            <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
            >
                Learn React
            </a>
        </Wrapper>
    )
}

export default Landing;
