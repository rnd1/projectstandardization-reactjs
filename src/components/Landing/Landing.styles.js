import styled, { keyframes } from "styled-components";

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

export const Wrapper = styled.div`
  text-align: center;
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;

  img {
    height: 40vmin;
    pointer-events: none;

    @media (prefers-reduced-motion: no-preference){
     animation: ${spin} infinite 20s linear;
    }
  }
 
  a {
   color: #61dafb;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',monospace;
  }
`

