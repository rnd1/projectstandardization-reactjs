import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

const BASE_URL = `https://${process.env.REACT_APP_API_URL}`

const HEADERS = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
}

const createRequest = (url) => ({ url, headers: HEADERS })

export const Auth = createApi({
    reducerPath: 'auth',
    baseQuery: fetchBaseQuery({BASE_URL}),
    endpoints: (builder) => ({
        login: builder.query({
            query: () => createRequest(`/pathToLogin`),
        }),
    }),
})

export const {
    useLogin,
} = Auth